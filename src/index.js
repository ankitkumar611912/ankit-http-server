const http = require("http");
const { v4: uuidv4 } = require("uuid");
const statusCode = require('http').STATUS_CODES

const server = http.createServer((req, res) => {
  if (req.method === "GET") {
    if (req.url === "/html") {
      res.writeHead(200, { "Content-Type": "text/html" });
      res.write(`
        <!DOCTYPE html>
        <html>
        <head>
        </head>
        <body>
            <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
            <p> - Martin Fowler</p>
        
        </body>
        </html>
        `);
      res.end();
    }
    else if (req.url === "/json") {
      res.writeHead(200, { "Content-Type": "application/json" });
      res.end(`{
            "slideshow": {
              "author": "Yours Truly",
              "date": "date of publication",
              "slides": [
                {
                  "title": "Wake up to WonderWidgets!",
                  "type": "all"
                },
                {
                  "items": [
                    "Why <em>WonderWidgets</em> are great",
                    "Who <em>buys</em> WonderWidgets"
                  ],
                  "title": "Overview",
                  "type": "all"
                }
              ],
              "title": "Sample Slide Show"
            }
          }`);
    }
    else if (req.url === "/uuid") {
      res.writeHead(200, { "Content-Type": "application/json" });
      const uuidVal = uuidv4();
      const uuidObj = {
        uuid: uuidVal,
      };
      res.end(JSON.stringify(uuidObj));
    }
    else if(req.url.includes('/status/')){
      let urlString = req.url;
      urlString = urlString.split('/status/');
      let code = urlString[1];
      
      if(statusCode.hasOwnProperty(code)){
        if(Number(code) >=200){
          res.writeHead(code);
        }
        res.end(statusCode[code]);
      }
      else{
        res.writeHead(404);
        res.end(`Worng status code given`)
      }
    }
    else if(req.url.includes('/delay/')){
      let urlString = req.url;
      urlString = urlString.split('/delay/');
      let delay = urlString[1];
      let delayTime = Number(delay);
      if(delayTime || delayTime === 0){
        setTimeout(()=>{
          res.end(statusCode[200])
        },delayTime*1000);
      }
      else{
        res.end('Worng input given')
      }
    }
    else{
      res.end('Invalid URL');
    }
  }
});

server.listen(8000);
